const decompress = require('decompress'),
	 		decompressUnzip = require('decompress-unzip'),
			request = require('request'),
			fs=require('fs'),
			path = require( 'path' ),
			process = require( "process" ),
			downloadSite = "https://bitbucket.org/ditrox/aht-2/";
let updateWindow;
const isDirectory = source => fs.lstatSync(source).isDirectory()
const getDirectories = source =>
	fs.readdirSync(source).map(name => path.join(source, name)).filter(isDirectory)
const isFile = source => fs.lstatSync(source).isFile()
const getFiles = source =>
	fs.readdirSync(source).map(name => path.join(source, name)).filter(isFile)


function moveEverything(moveFrom, moveTo) {
		var files=getFiles(moveFrom);
		for(var i=0;i<files.length;i++){
			var sp=files[i].split("\\");
			var toPath = path.join(moveTo, sp[sp.length-1]);
			fs.renameSync(files[i],toPath);
		}

		var dirs=getDirectories(moveFrom);
		for(var i=0;i<dirs.length;i++){
			var sp=dirs[i].split("\\");
			var toPath = path.join(moveTo, sp[sp.length-1]);
			if (!fs.existsSync(toPath)) {
					fs.mkdirSync(toPath);
			}
			moveEverything(dirs[i],toPath);
		}
		return true;
}
function rimraf(dir_path) {
    if (fs.existsSync(dir_path)) {
        fs.readdirSync(dir_path).forEach(function(entry) {
            var entry_path = path.join(dir_path, entry);
            if (fs.lstatSync(entry_path).isDirectory()) {
                rimraf(entry_path);
            } else {
                fs.unlinkSync(entry_path);
            }
        });
        fs.rmdirSync(dir_path);
    }
}

var BrowserWindow,ipcMain,session,app;
function updateEverything(e,link,config){
	if(link!=config["update"]){
		dir="ditrox-aht-2-"+link.replace(downloadSite+"get/","").replace(".zip","");
		request(link)
		.pipe(fs.createWriteStream(__dirname+'\\DXAHT.zip'))
		.on('close', function () {
			decompress(__dirname+'\\DXAHT.zip', __dirname+'\\tmp', {
			    plugins: [decompressUnzip()]
			}).then(() => {
				if(moveEverything(__dirname+"/tmp/"+dir,__dirname)){
					config["update"]=link;
					fs.writeFile(__dirname+'\\config.json',JSON.stringify(config), function(err) {
						app.relaunch({args: process.argv.slice(1).concat(['--relaunch'])})
						app.exit(0);
					});
				}
			});
		});
	} else {
		if(fs.existsSync(__dirname+"/tmp/")){
			rimraf(__dirname+"/tmp/");
		}
		if(fs.existsSync(__dirname+'\\DXAHT.zip')){
			fs.unlinkSync(__dirname+'\\DXAHT.zip');
		}
		session.fromPartition('ahtUpdater').clearCache(function(){
			session.fromPartition('ahtUpdater').clearStorageData(function(){
				if(updateWindow!=null){
					updateWindow.close();
					updateWindow=null;
				}
			});
		});
		return true;
	}
}

exports.initUpdate = function(bw,im,ses,a){
	var config;
	try{
		config=require(__dirname+"/config.json");
	}catch(e){
		config={"autoUpdaterEnabled":true,"update":""};
		fs.writeFileSync(__dirname+"/config.json",JSON.stringify(config),{encoding:'utf8',flag:'w'});
	}
	try{
		if(config["autoUpdaterEnabled"]==false){
			return true;
		} else{
			BrowserWindow=bw;
			ipcMain=im;
			session=ses;
			app=a;
			updateWindow = new BrowserWindow({
				show:false,
				webPreferences:{
					partition:"ahtUpdater",
					preload:__dirname+'\\updater.js'
				}
			})
			updateWindow.loadURL(downloadSite);
			return ipcMain.on("updateLink",function(event,link){
				updateEverything(event,link,config);
			});
		}
	}catch(e){
		session.defaultSession.clearCache(function(){
			if(updateWindow!=null){
				updateWindow.close();
				updateWindow=null;
			}
		});
		return true;
	}
}
