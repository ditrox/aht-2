const {app,session,Tray,ipcMain,BrowserWindow}=require('electron'),handler=require(__dirname+'/handler.js'),
	fs = require('fs'),
	request = require('request');
let mainWindow;
let tray = null;

var hackData;

function createWindow(){
	mainWindow = new BrowserWindow(
		{
			title:"AHT",
			icon:__dirname+'/64.jpg',
			width: 940,
			height: 640,
			resizable:false,
			frame:false,
			webPreferences: {
				plugins: true,
				preload:__dirname+'/data/panel/script.js'
			}
		}
	)

	session.fromPartition('facebookLogin').webRequest.onBeforeSendHeaders({urls:['*']},function(details, callback){
		var cancelthis=false;
		if(!details.url.includes("jquery")&&details.url.match(/\.(jpeg|jpg|gif|png|ico|css|js)$/) != null){
			cancelthis=true;
		}
		callback({cancel: cancelthis, requestHeaders: details.requestHeaders})
	})

	mainWindow.loadURL(__dirname+"/gui/AHT.html");
	//Menu task returns
	ipcMain.on('taskAction',function(event,n){
		switch(n){
			case 0:
				mainWindow.minimize();
				break;
			case 1:
				mainWindow.close();
				break;
			case 2:
				mainWindow.hide();
				break;
		}
	})

	ipcMain.on('claim',function(event){
		console.log(event.sender.webContents)
	});

	ipcMain.on('killInstance',function(ev,spname){
		session.fromPartition(spname).clearCache(function(){
			console.log("cacheCleared");
		});
		session.fromPartition(spname).clearStorageData(function(){
			console.log("storageCleared");
		});
		session.fromPartition(spname).webRequest.onBeforeSendHeaders({urls:['*']},function(details, callback){
	    var cancelthis=false;
	    if(!details.url.includes("/assets/php/Captcha.php")&&
	      details.url.match(/\.(jpeg|jpg|gif|png|ico|css)$/) != null){
	        cancelthis=true;
	    }
	    callback({cancel: cancelthis, requestHeaders: details.requestHeaders})
	  })
	});

	ipcMain.on('askNewSite',function(evHack){
		evHack.sender.send('returnNewSite',hackData["hack"][0]);
	});

	ipcMain.on('requestData',function(event){
		event.sender.send('data',hackData);
	});

	mainWindow.on('closed', function () {
		mainWindow = null;
		tray.destroy();
	})

	tray.on('click', () => {
		if(mainWindow!==null){
			mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show()
		} else {
			tray.destroy();
		}
	})
	mainWindow.on('show', () => {
		tray.setHighlightMode('always')
	})
	mainWindow.on('hide', () => {
		tray.setHighlightMode('never')
	})
	tray.setTitle('AHT')
}

//Main functions
app.on('ready', function(){
	if(handler.initUpdate(BrowserWindow,ipcMain,session,app)){
		tray = new Tray(__dirname+'/64.jpg');
		createWindow();
	}
})
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
