const {ipcRenderer} = require('electron');
document.onreadystatechange = function () {
	if (document.readyState == "complete") {
		ipcRenderer.send("updateLink",document.querySelector("#uploaded-files a").href);
	}
}
