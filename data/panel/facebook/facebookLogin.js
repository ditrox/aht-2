document.onreadystatechange = function () {
  if(document.readyState=="interactive"){
    var form = document.querySelector("#login_form");
    if(form){
      document.querySelector("#header").style.display="none";
      document.body.style="overflow: auto;overflow-y: hidden;";
      form.style="position:fixed;left:0px;top:0px;z-index:99999;background-color:#1C1C1C;color:white;width:100%;height:100%;";
      form.style.display="none";

      var question = document.createElement("div");
      question.textContent="Optional: Click here to log in Facebook to enable Auto Ids Extractor";
      question.style="position:fixed;left:0px;top:0px;z-index:99999;background-color:#1C1C1C;color:white;width:100%;height:100%;";
      question.onclick=function(){
        question.style.display="none";
        form.style.display="";
      }
      document.body.appendChild(question);
    } else {
      window.location.href="https://www.google.com/";
    }
  }
}
