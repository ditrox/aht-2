settings={
	"Dragon City":{
		fbpage:'DragonCity',
		inputF:["userId","sessionId","externalId"],
		url:"https://apps.facebook.com/dragoncity/",
		hacks:{
			"https://dc4u.eu/games/dragon-city":{}
		}
	},
	"Monster Legends":{
		fbpage:'MonsterLegends',
		inputF:["userId","sessionId","externalId"],
		url:"https://apps.facebook.com/monsterlegends/",
		hacks:{
			"https://dc4u.eu/games/monster-legends":{}
		}
	},
	"Farm Heroes Saga":{
		fbpage:'FarmHeroes',
		inputF:["","sessionKey",""],
		url:"https://apps.facebook.com/farmheroes/",
		hacks:{
			"https://dc4u.eu/games/farm-heroes":{}
		}
	},
	"Alphabetty Saga":{
		fbpage:'alphabettysaga',
		inputF:["","sessionKey",""],
		url:"https://apps.facebook.com/alphabettysaga/",
		hacks:{
			"https://dc4u.eu/games/alpha-betty":{}
		}
	},
	"Farm Heroes Super Saga":{
		fbpage:'FarmHeroesSuperSaga',
		inputF:["","sessionKey",""],
		url:"https://apps.facebook.com/farmheroessupersaga/",
		hacks:{
			"https://dc4u.eu/games/farm-heroes-super":{}
		}
	},
	"Criminal Case":{
		fbpage:'CriminalCaseGame',
		inputF:["Facebook Id","Signature",""],
		url:"https://apps.facebook.com/criminalcase/",
		hacks:{
			"https://dc4u.eu/games/criminal-case":{}
		}
	}
	/*"Candy Crush Saga":{
		fbpage:'candycrushsaga',
		inputF:["","sessionKey",""],
		url:"https://apps.facebook.com/candycrush/"
	},
	"Candy Crush Soda Saga":{
		fbpage:'CandyCrushSodaSaga',
		inputF:["","sessionKey",""],
		url:"https://www.facebook.com/games/candycrushsoda/"
	},
	"Diamond Digger Saga":{
		fbpage:'diamonddiggersaga',
		inputF:["","sessionKey",""],
		url:"https://apps.facebook.com/diamonddiggersaga/"
	},
	"Papa Pear Saga":{
		fbpage:'PapaPearSaga',
		inputF:["","sessionKey",""],
		url:"https://apps.facebook.com/papapear/"
	},
	"Bubble Witch Saga":{
		fbpage:'BubbleWitchSaga',
		inputF:["","sessionKey",""],
		url:"https://apps.facebook.com/bubblewitch/"
	},
	"Bubble Witch 2 Saga":{
		fbpage:'bubblewitchsaga2',
		inputF:["","sessionKey",""],
		url:"https://apps.facebook.com/bubblewitch-two/"
	},
	"Pepper Panic Saga":{
		fbpage:'PepperPanic',
		inputF:["","sessionKey",""],
		url:"https://apps.facebook.com/pepperpanicsaga/"
	},
	"Bubble Coco":{
		fbpage:'playbubblecoco',
		inputF:["User Id","Signed Request",""],
		url:"https://apps.facebook.com/bubblecoco/"
	},
	"Buggle":{
		fbpage:'talkbuggle',
		inputF:["User Id","Signed Request",""],
		url:"https://apps.facebook.com/playbuggle/"
	},
	"Pengle":{
		fbpage:'talkpengle',
		inputF:["User Id","Signed Request",""],
		url:"https://apps.facebook.com/playpengle/"
	},
	"Pig & Dragon":{
		fbpage:'talkpiganddragon',
		inputF:["User Id","Signed Request",""],
		url:"https://apps.facebook.com/playpigdragon/"
	},
	"Color Pop":{
		fbpage:'talkcolorpop',
		inputF:["User Id","Signed Request",""],
		url:"https://apps.facebook.com/playcolorpop/"
	},
	"Pudding Pop":{
		fbpage:'playpudding',
		inputF:["User Id","Signed Request",""],
		url:"https://apps.facebook.com/playpudding/"
	},
	"Tasty Pop":{
		fbpage:'talktastypop',
		inputF:["User Id","Signed Request",""],
		url:"https://apps.facebook.com/playtastypop/"
	},
	"Solitaire in Wonderland":{
		fbpage:'talksolitaireinwonderland',
		inputF:["User Id","Signed Request",""],
		url:"https://apps.facebook.com/solitaireinwonder/"
	}*/
};
