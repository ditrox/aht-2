const {ipcRenderer} = require('electron');
require(__dirname+"/config.js");
const fs = require('fs');
const app = require('electron').remote.app;

var basepath = app.getAppPath();

var IamInterval;
function changeStatus(text,i){
	clearInterval(IamInterval);
	document.querySelector("#status").getElementsByTagName("p")[i].textContent=text;
	if(text.includes("...")){
		IamInterval=setInterval(function(){
			if(document.querySelector("#status").textContent.split(".").length==4){
				document.querySelector("#status").textContent=document.querySelector("#status").textContent.replace("...",".");
			} else if(document.querySelector("#status").textContent.split(".").length==3){
				document.querySelector("#status").textContent=document.querySelector("#status").textContent.replace("..","...");
			} else{
				document.querySelector("#status").textContent=document.querySelector("#status").textContent.replace(".","..");
			}
		},500);
	}
}

function loadForm(container){
	var game=document.querySelector("#gameName").textContent;
	document.querySelector("#webHackTools").innerHTML='';
	for(var key in settings[game]["hacks"]){
		var option = document.createElement("option");
		option.value=key;
		option.textContent=key;
		document.querySelector("#webHackTools").appendChild(option);
	}
	fs.writeFileSync(__dirname+"/hacktool/data.json",JSON.stringify({}),{encoding:'utf8',flag:'w'});
	createWebTag([	["preload","hacktool/loadHacks.js"],
		 ["partition","persist:facebookLogin"],
		 ["id","webHack"],
		 ["src","http://m.facebook.com/home.php?_rdr"],
		 ["style","width:350px;height:200px;"]],
		 "#formWeb");
	 fs.writeFileSync(__dirname+"/hacktool/result.txt","No claim yet",{encoding:'utf8',flag:'w'});

		setInterval(function(){
			var newtext=fs.readFileSync(__dirname+"/hacktool/result.txt", "utf8");
			if(newtext.replace(/<br>/g,"") != document.querySelector("#result div").textContent){
				document.querySelector("#result div").innerText=newtext.replace(/<br>/g,"\n");
			}
		},500);
		if(require(__dirname+"/fbloged.json")["status"]){
			webHack.src=settings[game]["url"];
			var checkLogin2=setInterval(function(){
				if(document.querySelector("#webHack").src.includes("bing")){
						clearInterval(checkLogin2);
						webHack.src=document.querySelector("#webHackTools").value;
				}
			},1000);
		} else {
			webHack.src=document.querySelector("#webHackTools").value;
		}
	 document.querySelector("#webHackTools").onchange=function(){
		 document.querySelector("#webHack").src=this.value;
	 }
	 changeStatus("Waiting for you",0);
	 function some(i){
		 var a=createWebTag([	["preload","hacktool/webview.js"],
				["partition","sessionHackTool"+i],
				["id","webHack"+i],
				["src",document.querySelector("#webHackTools").value],
				["style","width:1px;height:1px;"]],
				"#webviews")
				a.addEventListener('will-navigate', (event) => {
					changeStatus("Surfing "+event.url,i);
					if(event.url.includes("twitter")){
						document.querySelector("#webviews").removeChild(document.querySelector("#webHack"+i));
						some(i);
					}
				});
				a.addEventListener('destroyed', (event) => {
					some(i);
				});
				ipcRenderer.send('killInstance',"sessionHackTool"+i);
	 }
	 var loadingHack=setInterval(function(){
	 	var object = JSON.parse(fs.readFileSync(__dirname+"/hacktool/data.json", "utf8"));
	 	if(Object.keys(object).length != 0){
	 		clearInterval(loadingHack);

			 for(var i=0;i<document.querySelector("#instances").value;i++){
				some(i);
				var newP=document.createElement("p");
				document.querySelector("#status").appendChild(newP);
			}
	 	}
	 },500);

}


function createGameMenu(game){
	var gameDiv = document.createElement("div");
	gameDiv.setAttribute("class","game");

	gameDiv.onclick=function(){
		document.querySelector("#gameName").textContent=this.textContent;
		document.querySelector("#SearchGame").style.display="none";
		document.querySelector("#HackGame").style.display="";
		loadForm("form");
	}


	document.querySelector(".result").appendChild(gameDiv);

	var gameImage = document.createElement("img");
	gameImage.width=64;
	gameImage.height=64;
	if(settings[game].fbpage){
		gameImage.src='https://graph.facebook.com/'+settings[game].fbpage+'/picture';
	} else {
		gameImage.src=settings[game].icon;
	}
	gameDiv.appendChild(gameImage);

	var gameName= document.createElement("span");
	gameName.textContent=game;
	gameDiv.appendChild(gameName);
}

function updateGameList(games,word){
	document.querySelector(".result").innerHTML="";
	var counter=0;
	for(game in games){
		if(counter==8){
			break;
		}
		if(word=='' || games[game].toLowerCase().includes(word.toLowerCase())){
			createGameMenu(games[game]);
			counter++;
		}

	}
}

var userAgents=[
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:53.0) Gecko/20100101 Firefox/53.0"
	/*"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393",
	"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)",
	"Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)",
	"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)",
	"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0;  Trident/5.0)",
	"Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0; MDDCJS)",
	"Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko",
	"Mozilla/5.0 (iPad; CPU OS 8_4_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12H321 Safari/600.1.4",
	"Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1",
	"Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)",
	"Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)",
	"Mozilla/5.0 (Linux; Android 6.0.1; SAMSUNG SM-G570Y Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/4.0 Chrome/44.0.2403.133 Mobile Safari/537.36",
	"Mozilla/5.0 (Linux; Android 5.0; SAMSUNG SM-N900 Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/2.1 Chrome/34.0.1847.76 Mobile Safari/537.36",
	"Mozilla/5.0 (Linux; Android 6.0.1; SAMSUNG SM-N910F Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/4.0 Chrome/44.0.2403.133 Mobile Safari/537.36",
	"Mozilla/5.0 (Linux; U; Android-4.0.3; en-us; Galaxy Nexus Build/IML74K) AppleWebKit/535.7 (KHTML, like Gecko) CrMo/16.0.912.75 Mobile Safari/535.7",
	"Mozilla/5.0 (Linux; Android 7.0; HTC 10 Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.83 Mobile Safari/537.36",
	"curl/7.35.0",
	"Wget/1.15 (linux-gnu)",
	"Lynx/2.8.8pre.4 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.12.23"*/
];

function createWebTag(attributes,where,ua=null){
	var newWebView=document.createElement("webview");
	for(a=0;a<attributes.length;a++){
		if(attributes[a][0]=="preload"){
			attributes[a][1]="file://"+__dirname+"/"+attributes[a][1];
		}
		newWebView.setAttribute(attributes[a][0],attributes[a][1]);
	}
	if(ua==null){
		ua=userAgents[Math.floor(Math.random()*userAgents.length)];
	}
	newWebView.setAttribute("nodeIntegration",false);
	newWebView.setAttribute("useragent",ua);
	newWebView.setAttribute("disablewebsecurity",true);
	newWebView.setAttribute("allowpopups",false);
	newWebView.setAttribute("webpreferences","allowRunningInsecureContent,nodeIntegration=no");


	newWebView.addEventListener('dom-ready', () => {
		if(ua==null){
			newWebView.setUserAgent();
		} else {
			newWebView.setUserAgent(ua);
		}
  })

	document.querySelector(where).appendChild(newWebView);
	return newWebView;
}

function init(){
	document.querySelector("#minimize").onclick=function(){
		ipcRenderer.send("taskAction",0)
	};
	document.querySelector("#close").onclick=function(){
		ipcRenderer.send("taskAction",1)
	};
	document.querySelector("#tray").onclick=function(){
		ipcRenderer.send("taskAction",2)
	};

	createWebTag([	["preload","facebook/facebookLogin.js"],
		 ["partition","persist:facebookLogin"],
		 ["id","facebookLoginWebView"],
		 ["src","http://m.facebook.com/home.php?_rdr"],
		 ["style","width:100%;height:150px;"]],
		 "#facebookLogin",userAgents[0]);
	var checkLogin=setInterval(function(){
		if(document.querySelector("#facebookLoginWebView").src.includes("google")){
			fs.writeFileSync(__dirname+"/fbloged.json",JSON.stringify({status:true}),{encoding:'utf8',flag:'w'});
			clearInterval(checkLogin);
			document.querySelector("#facebookLogin").innerHTML="";
		} else {
			fs.writeFileSync(__dirname+"/fbloged.json",JSON.stringify({status:false}),{encoding:'utf8',flag:'w'});
		}
	},1000);
	var games=[];
	for(key in settings){
		games.push(key);
	}
	games.sort();
	document.querySelector("#search").onchange=function(){
		updateGameList(games,this.value);
	}
	document.querySelector("#search").onkeydown=function(){
		updateGameList(games,this.value);
	}
	document.querySelector("#search").onkeyup=function(){
		updateGameList(games,this.value);
	}
	document.querySelector("#autoupdaterStatus").textContent=require(basepath+"\\config.json")["autoUpdaterEnabled"];
	document.querySelector("#autoUpdaterChange").onclick=function(){
		var AHTconfig=require(basepath+"\\config.json");
		if(AHTconfig["autoUpdaterEnabled"]===true){
			AHTconfig["autoUpdaterEnabled"]=false;
		} else {
			AHTconfig["autoUpdaterEnabled"]=true;
		}
		fs.writeFile(basepath+'\\config.json',JSON.stringify(AHTconfig), function(err) {
			document.querySelector("#autoupdaterStatus").textContent=AHTconfig["autoUpdaterEnabled"];
		});
	}
	document.querySelector("#SearchGame").style.display="";
}

document.onreadystatechange = function () {
	if (document.readyState == "complete") {
		init();
	}
}
