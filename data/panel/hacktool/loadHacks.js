var sr;
try{
  sr = require(__dirname+'\\signedrequest.json');
} catch(e){
  sr = {"value":false};
}
console.log(sr["value"]);

document.onreadystatechange = function () {
  if(document.readyState=="interactive"){

    var parent=null;
    var hackURL=document.URL;
    switch(document.domain){
    	case "vina-full.com":
    	case "dc4u.eu":
      case "www.ha-lab.com":
        window.stop();

        parent = document.querySelector('#ActionForm');
        if(sr["value"]!=false){
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              require("fs").writeFileSync(__dirname+"/result.txt",JSON.parse(this.responseText)["Result"],{encoding:'utf8',flag:'w'});
            }
          };
          xhttp.open("POST", "https://dc4u.eu/assets/php/Actions/GetInfo.php", true);
          xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhttp.send("sign="+sr["value"]+"&game="+parent.querySelector("#Game").value);
        }
        document.body.style="overflow: auto;overflow-y: hidden;";
        parent.querySelector('input[name="captchaKey"]').parentElement.parentElement.style.display="none";
        parent.querySelector("#showInfo").parentElement.parentElement.style.display="none";
        parent.querySelector('input[value="Submit"]').parentElement.parentElement.style.display="none";
        parent.style="position:fixed;left:0px;top:0px;z-index:99999;background-color:#1C1C1C;color:white;width:350px;height:200px;overflow:auto;overflow-y:scroll";
    		break;
      case "haxclick.com":
        window.stop();
        var form = document.querySelector('form[method="post"][action="login.php"]');
        if(form){
          form.getElementsByTagName("button")[0].click();
        }
        hackURL="http://haxclick.com/";
        parent = document.querySelector('#dc_hack_form');
        document.body.style="overflow: auto;overflow-y: hidden;";
        parent.querySelector('input[name="captcha_code"]').parentElement.parentElement.style.display="none";
        parent.getElementsByClassName("card-title")[0].style.display="none";
        parent.getElementsByTagName("hr")[0].style.display="none";
        parent.querySelector("#submit_box").style.display="none";
        parent.querySelector('input[name="auto"]').parentElement.parentElement.style.display="none";
        parent.style="position:fixed;left:0px;top:0px;z-index:99999;background-color:#1C1C1C;color:white;width:250px;height:100%;";
        break;
      case "facebook.com":
        if(document.location.host!="m.facebook.com"){
          var signedrequest;
          try{
            signedrequest = document.getElementsByName("signed_request")[0].value;
          }catch(e){
            signedrequest = false;
          }
          require("fs").writeFileSync(__dirname+"/signedrequest.json",JSON.stringify({value:signedrequest}),{encoding:'utf8',flag:'w'});
          window.location.href="https://www.bing.com/";
        }
        break;
    }
    if(parent!=null){
      var button = document.createElement("input");
      button.setAttribute("type","submit");
      button.style="color:black;font-size:18px;";
      button.value="Start";
      button.onclick=function(){
        var result={};
        var blocks = parent.getElementsByClassName("form-group");
        for(var i=0;i<blocks.length;i++){
          if(blocks[i].parentElement.style.display==="" && blocks[i].style.display===""){
              var obj=blocks[i].children[1];
              result[obj.getAttribute("name")]=obj.value;
            }
        }
        result["hackURL"] = hackURL;
        require('fs').writeFileSync(__dirname+"/data.json",JSON.stringify(result),{encoding:'utf8',flag:'w'});
      }
      parent.appendChild(button);
    }
  }
}
