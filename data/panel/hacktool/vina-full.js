
const {solveCaptcha} = require(__dirname+'/captcha.js');
var dataHack = require(__dirname+'/data.json');

function beforeLoad(){
  if(document.URL=="https://"+document.domain+"/redirect.html"){
    setTimeout(function(){
    window.location.href="/adStart";
    },1000);
  } else if(document.URL=="https://"+document.domain+"/adStart"){
    setTimeout(function(){
    window.location.href="https://"+document.domain+"/redirect.html";
    },10000);
  }
}
function form(){
  setTimeout(function(){
    try{
      var container = document.querySelector("#ActionForm");
      var captchaImage = container.querySelector('img[src="/assets/php/Captcha.php"]');
      if(container.querySelector('a[href="/redirect.html"]') && container.querySelector('a[href="/redirect.html"]').style.display!="none"){
        container.querySelector('a[href="/redirect.html"]').click();
      } else{
          if(container.querySelector("#showInfo").checked==false){
            container.querySelector("#showInfo").click();
          }
          for(var k in dataHack){
            try{
              if(k!="hackURL"){
                container.querySelectorAll('[name="'+k+'"]')[0].value = dataHack[k];
              }
            }catch(e){
            }
          }
          container.querySelector('input[name="captchaKey"]').value=solveCaptcha(captchaImage);
          setTimeout(function(){
            var result = document.querySelector("body > div.wrapper > div.content-wrapper > section > div.row > div.col-lg-4 > div > div.box-body.dlHackInfo").textContent;
            document.querySelector('input[value="Submit"]').click();
            var check=setInterval(function(){
              if(result !=  document.querySelector("body > div.wrapper > div.content-wrapper > section > div.row > div.col-lg-4 > div > div.box-body.dlHackInfo").textContent){
                clearInterval(check);
                var d = new Date();
                var n = d.getTime();
                require('fs').writeFileSync(__dirname+"/result.txt",n+"\n"+document.querySelector("body > div.wrapper > div.content-wrapper > section > div.row > div.col-lg-4 > div > div.box-body.dlHackInfo").textContent,{encoding:'utf8',flag:'w'})
              //  ipcRenderer.send('killInstance',null);
                window.location.href="https://www.twitter.com/";
              }
            },500);
        },500);
      }
    }catch(e){
      alert(e.message);
    }
  },2500);
}

exports.init = function(status){
  switch(status){
    case "beforeLoad":
      if(document.URL!==dataHack["hackURL"]){
        beforeLoad();
      }
      break;
  //  case "idle":
  //    break;
    case "interactive":
    //case "complete":
      if(document.URL===dataHack["hackURL"]){
        var container = document.querySelector("#ActionForm");
        container.setAttribute("novalidate","true");
        var captchaImage = container.querySelector('img[src="/assets/php/Captcha.php"]');
        if(captchaImage.complete){
          form();
        } else {
          captchaImage.onload=function(){
            form();
          }
        }
      }
      break;
  }
}
